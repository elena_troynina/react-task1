/**
 * Created by user on 29.11.18.
 */
import React, { Component } from 'react';
import TimeComponent from './TimeComponent';

class GreetingComponent extends Component {
    render() {
        return (
            <p>Time is <TimeComponent /></p>
        );
    }
}

export default GreetingComponent;